DROP DATABASE IF EXISTS `demo`;
CREATE DATABASE `demo` CHAR SET =utf8mb4;
USE `demo`;
DROP TABLE IF EXISTS `user`;
CREATE TABLE `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_name` varchar(20) DEFAULT NULL COMMENT '用户名',
  `user_password` varchar(100) DEFAULT NULL COMMENT '密码',
  `user_gender` char(1) DEFAULT NULL COMMENT '性别',
  `user_email` varchar(100) DEFAULT NULL COMMENT '邮箱地址,用于找回密码',
  `is_delete` int(11) DEFAULT NULL COMMENT '是否删除',
  UNIQUE KEY `id` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

INSERT INTO `user`(user_name, user_password, user_gender, user_email, is_delete)
VALUES ('小惠','123456','女','123456@qq.com',0),
  ('小篮','123456','女','123789456@qq.com',0)