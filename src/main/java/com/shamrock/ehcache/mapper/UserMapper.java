package com.shamrock.ehcache.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.shamrock.ehcache.entity.UserEntity;
import org.springframework.stereotype.Repository;

@Repository
public interface UserMapper extends BaseMapper<UserEntity> {
}
