package com.shamrock.ehcache.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.io.Serializable;

@Data
@TableName(value = "user")
public class UserEntity implements Serializable {
    private String userName;
    private String userPassword;
    private String userGender;
    private String userEmail;
}
