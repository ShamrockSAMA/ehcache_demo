package com.shamrock.ehcache.service.impl;

import com.shamrock.ehcache.entity.UserEntity;
import com.shamrock.ehcache.mapper.UserMapper;
import com.shamrock.ehcache.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;
import java.util.List;

@Service
public class UserServiceImpl implements UserService {
    @Autowired
    private UserMapper userMapper;

    @Override
    @Cacheable(value = "ShamrockCache")
    public List<UserEntity> listUserAll() {
        List<UserEntity> userEntityList = userMapper.selectByMap(null);
        return userEntityList;
    }
}
