package com.shamrock.ehcache.service;

import com.shamrock.ehcache.entity.UserEntity;
import java.util.List;

public interface UserService {
    List<UserEntity> listUserAll();
}
