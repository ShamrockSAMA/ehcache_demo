package com.shamrock.ehcache.controller;

import com.alibaba.fastjson.JSON;
import com.shamrock.ehcache.entity.UserEntity;
import com.shamrock.ehcache.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

@Controller
public class UserController {
    @Autowired
    private UserService userService;

    @ResponseBody
    @RequestMapping(value = "/getUserAll",method = RequestMethod.GET)
    public String getUserAll(){
        List<UserEntity> userEntityList = userService.listUserAll();
        return JSON.toJSONString(userEntityList);
    }
}
