package com.shamrock.ehcache.config;

import lombok.Data;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

@Data
@Configuration
@PropertySource(value = "classpath:myDataSource.properties")
public class MyDataBaseSourceConfig {
    @Value(value = "${jdbc.userName}")
    private String userName;
    @Value(value = "${jdbc.password}")
    private String password;
    @Value(value = "${jdbc.url}")
    private String url;
    @Value(value = "${jdbc.driverName}")
    private String driverClassName;
}
