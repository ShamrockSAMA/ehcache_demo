package com.shamrock.ehcache.config;

import com.alibaba.druid.pool.DruidDataSource;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import javax.sql.DataSource;

@Configuration
@MapperScan("com.shamrock.ehcache.mapper")
public class MyDataBaseConfig {
    @Autowired
    private MyDataBaseSourceConfig myDataBaseSourceConfig;

    @Bean
    public DataSource dataSource(){
        DruidDataSource druidDataSource = new DruidDataSource();
        druidDataSource.setUsername(myDataBaseSourceConfig.getUserName());
        druidDataSource.setPassword(myDataBaseSourceConfig.getPassword());
        druidDataSource.setUrl(myDataBaseSourceConfig.getUrl());
        druidDataSource.setDriverClassName(myDataBaseSourceConfig.getDriverClassName());
        return druidDataSource;
    }
}
